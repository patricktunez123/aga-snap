# Aga snap

## DEMO

Click [here](https://aga-snap.vercel.app/) to view this app's demo

## Technologies used

- ReactJS
- Redux
- Bootstrap
- Antd
- Sass
- Firebase

## Dependencies

- "@testing-library/jest-dom": "^5.11.4",
- "@testing-library/react": "^11.1.0",
- "@testing-library/user-event": "^12.1.10",
- "antd": "^4.16.13",
- "bootstrap": "^5.1.0",
- "emailjs": "^3.5.0",
- "emailjs-com": "^3.2.0",
- "firebase": "^8.8.1",
- "moment": "^2.29.1",
- "react": "^17.0.2",
- "react-dom": "^17.0.2",
- "react-helmet": "^6.1.0",
- "react-icons": "^4.2.0",
- "react-redux": "^7.2.4",
- "react-router-dom": "^5.2.1",
- "react-scripts": "4.0.3",
- "redux": "^4.1.1",
- "redux-devtools-extension": "^2.13.9",
- "redux-thunk": "^2.3.0",
- "sass": "^1.38.2",
- "web-vitals": "^1.0.1"

## How to set up this app on your local invironment

These instructions will get you a copy of this project up and running on your local machine.

## Prerequisites

To install this project on your local machine, you need first to clone the repository `https://gitlab.com/patricktunez123/aga-snap.git` or download the zip file and once this is set up you're going to need NODEJS installed on your machine.

## Installing

The installation of this application is straightforward, After cloning this repository to your local machine, cd into it using your terminal and run the following command

- yarn

It will install all the node modules for the project.

## Available scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Features

- Required form fields should be filled
- Email should be full
- Password should at least be 6 characters with one upper case alphabet
- Username should be different from any other snapchat user
- Retrieve password link should be sent to the email the user created the account with
- View the friends i have on snapchat
- View profile details
- Ask permission to access phone camera
- Cannot change username
- Change avatar
- View my friends' stories
- Change password
- Change snapchat name

## Not done yet features

- Ask permission to use filters when taking pictures or videos.
- Save or delete taken pictures or videos from snapchat or gallery.
- Ask permission to access phone gallery

# Author

Patrick TUNEZERWANE, +250781429268

- [Linkedin](https://www.linkedin.com/in/patrick-tunezerwane-0a901ba8/)
- [Twitter](https://twitter.com/tunezpatrick)

---

## License

MIT License

Copyright (c) 2021 Patrick TUNEZERWANE

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
