import { combineReducers } from "redux";
import {
  getUsers,
  postUser,
  resetPasswordRequest,
  updateUser,
} from "./user.reducers";
import { getFriends } from "./friend.reducers";
import { Auth } from "./auth.reducers";

export const reducers = combineReducers({
  Users: getUsers,
  PostUser: postUser,
  UpdateUser: updateUser,
  PasswordReset: resetPasswordRequest,
  Friends: getFriends,
  Auth: Auth,
});
