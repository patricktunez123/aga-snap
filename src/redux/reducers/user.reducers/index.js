import { userActionTypes } from "../../actionTypes";

const initialState = {
  loading: false,
  errorMessage: null,
  successMessage: null,
  data: [],
};

export const getUsers = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case userActionTypes.GET_USERS_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case userActionTypes.GET_USERS_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };

    case userActionTypes.GET_USERS_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

export const postUser = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case userActionTypes.ADD_USER_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case userActionTypes.ADD_USER_SUCCESS:
      return {
        ...prevState,
        loading: false,
        successMessage: payload,
      };

    case userActionTypes.ADD_USER_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

export const resetPasswordRequest = (
  prevState = initialState,
  { type, payload }
) => {
  switch (type) {
    case userActionTypes.RESET_PASSWORD_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case userActionTypes.RESET_PASSWORD_SUCCESS:
      return {
        ...prevState,
        loading: false,
        successMessage: payload,
      };

    case userActionTypes.RESET_PASSWORD_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

export const updateUser = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case userActionTypes.UPDATE_USER_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case userActionTypes.UPDATE_USER_SUCCESS:
      return {
        ...prevState,
        loading: false,
        successMessage: payload,
      };

    case userActionTypes.UPDATE_USER_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};
