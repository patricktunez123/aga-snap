import { friendActionTypes } from "../../actionTypes";

const initialState = {
  loading: false,
  errorMessage: null,
  successMessage: null,
  data: [],
};

export const getFriends = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case friendActionTypes.GET_FRIENDS_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case friendActionTypes.GET_FRIENDS_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };

    case friendActionTypes.GET_FRIENDS_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};
