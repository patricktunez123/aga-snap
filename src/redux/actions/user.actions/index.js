import { db } from "../../../config/firebase";
import { userActionTypes } from "../../actionTypes";
import emailjs from "emailjs-com";
import moment from "moment";

export const getUsers = () => async (dispatch) => {
  try {
    dispatch({
      type: userActionTypes.GET_USERS_REQUEST,
    });

    await db.collection("users").onSnapshot((snapshot) => {
      const data = snapshot.docs.map((doc) => ({
        id: doc.id,
        email: doc?.data().email,
        firstName: doc?.data().firstName,
        lastName: doc?.data().lastName,
        password: doc?.data().password,
        phoneNumber: doc?.data().phoneNumber,
        username: doc?.data().username,
      }));
      dispatch({
        type: userActionTypes.GET_USERS_SUCCESS,
        payload: data,
      });
    });
  } catch (error) {
    dispatch({
      type: userActionTypes.GET_USERS_FAIL,
      payload: error,
    });
  }
};

export const postUser = (data) => async (dispatch) => {
  try {
    dispatch({
      type: userActionTypes.ADD_USER_REQUEST,
    });
    await db.collection("users").add({
      firstName: data?.firstName,
      lastName: data?.lastName,
      email: data?.email,
      password: data?.password,
      username: data?.username,
      timestamp: moment(new Date().getTime()).format("MMMM Do YYYY, h:mm:ss a"),
    });

    dispatch({
      type: userActionTypes.ADD_USER_SUCCESS,
      payload: "Signup was successful",
    });
  } catch (error) {
    dispatch({
      type: userActionTypes.ADD_USER_FAIL,
      payload: error,
    });
  }
};

export const resetRequest = (data) => async (dispatch) => {
  try {
    dispatch({
      type: userActionTypes.RESET_PASSWORD_REQUEST,
    });

    const tempParams = {
      email: data?.email,
      timestamp: moment(new Date().getTime()).format("MMMM Do YYYY, h:mm:ss a"),
    };

    await emailjs.send(
      "service_yxw3y6k",
      "template_ll6e6hg",
      tempParams,
      "user_WM1TjobNmh7ymbvkODNmk"
    );

    dispatch({
      type: userActionTypes.RESET_PASSWORD_SUCCESS,
      payload:
        "Link to reset your password has been sent to your email, go check it out",
    });
  } catch (error) {
    dispatch({
      type: userActionTypes.RESET_PASSWORD_FAIL,
      payload: "There was an error, please try again",
    });
  }
};
export const updateUser = (id, values) => async (dispatch) => {
  try {
    dispatch({
      type: userActionTypes.UPDATE_USER_REQUEST,
    });

    await db.collection("users").doc(id).update(values);
    dispatch({
      type: userActionTypes.UPDATE_USER_SUCCESS,
      payload: "Profile is updated successflly",
    });
  } catch (error) {
    dispatch({
      type: userActionTypes.UPDATE_USER_FAIL,
      payload: error,
    });
  }
};
