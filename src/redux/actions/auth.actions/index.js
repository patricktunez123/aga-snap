import { authActionTypes } from "../../actionTypes";
import { db } from "../../../config/firebase";

export const Auth = (email, password) => async (dispatch) => {
  try {
    dispatch({
      type: authActionTypes.LOGIN_REQUEST,
    });

    const snapshot = await db.collection("users").get();
    const data = snapshot?.docs?.map((doc) => doc.data());
    data?.filter((user) => {
      if (user?.email === email || user?.password === password) {
        const saveToLocal = localStorage.setItem("user", JSON.stringify(user));
        dispatch({
          type: authActionTypes.LOGIN_SUCCESS,
          payload: saveToLocal,
        });
      }
      return null;
    });
  } catch (error) {
    dispatch({
      type: authActionTypes.LOGIN_FAIL,
      payload: error,
    });
  }
};
