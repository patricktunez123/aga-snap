import { db } from "../../../config/firebase";
import { friendActionTypes } from "../../actionTypes";

export const getFriends = () => async (dispatch) => {
  try {
    dispatch({
      type: friendActionTypes.GET_FRIENDS_REQUEST,
    });

    await db.collection("friends").onSnapshot((snapshot) => {
      const data = snapshot.docs.map((doc) => ({
        id: doc.id,
        names: doc?.data().names,
        photoUrl: doc?.data().photoUrl,
        username: doc?.data().username,
      }));
      dispatch({
        type: friendActionTypes.GET_FRIENDS_SUCCESS,
        payload: data,
      });
    });
  } catch (error) {
    dispatch({
      type: friendActionTypes.GET_FRIENDS_FAIL,
      payload: error,
    });
  }
};
