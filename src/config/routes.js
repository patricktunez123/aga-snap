export const routes = {
  createAccount: {
    name: "createAccount",
    url: "/signup",
  },
  Login: {
    name: "Login",
    url: "/",
  },
  Home: {
    name: "Home",
    url: "/home",
  },
  Friends: {
    name: "Friends",
    url: "/friends",
  },
  Chat: {
    name: "Chat",
    url: "/chat",
  },
  Profile: {
    name: "Profile",
    url: "/profile",
  },
  PasswordResetRequest: {
    name: "PasswordResetRequest",
    url: "/password_reset_request",
  },
  ResetPassword: {
    name: "ResetPassword",
    url: "/reset-password",
  },
};
