import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyBHknzSW8t1QDfHf_YLBDnXpBjsjZJUkAE",
  authDomain: "agasnap-697ed.firebaseapp.com",
  projectId: "agasnap-697ed",
  storageBucket: "agasnap-697ed.appspot.com",
  messagingSenderId: "242971272216",
  appId: "1:242971272216:web:805f36ac49595ec8668f6d",
};

const app = firebase.initializeApp(firebaseConfig);
export const db = app.firestore();
export const storage = app.storage();
