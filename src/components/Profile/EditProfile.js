import React, { useEffect, useState } from "react";
import { Drawer, Form } from "antd";
import { useDispatch } from "react-redux";
import EditProfileForm from "../Forms/EditProfileForm";
import { updateUser } from "../../redux/actions/user.actions";

const EditProfile = ({ onClose, visible }) => {
  const dispatch = useDispatch();
  const [values, setValues] = useState();
  const [form] = Form.useForm();

  const onFinish = (values) => {
    setValues(values);
    form.resetFields();
  };

  let loggedInUser = JSON.parse(localStorage.getItem("user"));

  const data = {
    username: loggedInUser?.username,
    firstName: loggedInUser?.firstName,
    lastName: loggedInUser?.lastName,
    password: loggedInUser?.password,
  };

  useEffect(() => {
    form.setFieldsValue(data);
  }, [form, data]);

  const handleUpdate = () => {
    dispatch(updateUser(data?.username, values));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <Drawer
      title="Edit profile"
      width={450}
      placement="right"
      closable={true}
      onClose={onClose}
      visible={visible}
    >
      <EditProfileForm
        form={form}
        initialValues={loggedInUser}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        handleUpdate={handleUpdate}
      />
    </Drawer>
  );
};

export default EditProfile;
