import React from "react";
import { MdPersonAdd } from "react-icons/md";

const FriendsToAdd = () => {
  return (
    <div className="list-item">
      <div className="right">
        <img
          alt=""
          src="https://images.pexels.com/photos/771742/pexels-photo-771742.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
        />
        <div className="user-info">
          <span className="font-500">Lewis</span>
          <span className="text-muted small-text">lewis_2021</span>
        </div>
      </div>
      <div>
        <MdPersonAdd className="cursor font-500" />
      </div>
    </div>
  );
};

export default FriendsToAdd;
