import React, { useState } from "react";
import { Modal } from "antd";
import { HiOutlineCamera } from "react-icons/hi";

const Contact = ({ names, photoUrl }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <div className="list-item">
        <div onClick={showModal} className="right cursor">
          <img
            alt=""
            src={
              photoUrl
                ? photoUrl
                : "https://images.pexels.com/photos/771742/pexels-photo-771742.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
            }
          />
          <div className="user-info">
            <span className="font-500">{names}</span>
            <span className="text-muted small-text">View stories</span>
          </div>
        </div>
        <div>
          <HiOutlineCamera className="cursor font-500" />
        </div>
      </div>
      <Modal
        title="Diane stories"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={false}
      >
        <h6>Hey yo what's up guys!</h6>
        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries,.
        </p>
      </Modal>
    </>
  );
};

export default Contact;
