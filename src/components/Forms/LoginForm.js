import React from "react";
import { Form, Input, Button, message } from "antd";
import { Link } from "react-router-dom";
import { routes } from "../../config/routes";

const Login = ({ errorMessage, onFinish }) => {
  if (errorMessage) {
    message.error(errorMessage);
  }
  return (
    <Form name="basic" onFinish={onFinish}>
      <div className="row">
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Email can not be empty",
              },
            ]}
          >
            <Input
              className="agasnap--input"
              size="large"
              placeholder="Email"
            />
          </Form.Item>
        </div>
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Password can not be empty",
              },
            ]}
          >
            <Input.Password
              className="agasnap--input"
              size="large"
              placeholder="Password"
            />
          </Form.Item>
        </div>
        <div className="col-lg-6 col-md-6 col-12 offset-md-6">
          <Link to={routes.PasswordResetRequest.url}>Forgot Password</Link>
        </div>

        <div className="col-lg-4 col-md-4 col-12 offset-md-5">
          <Form.Item>
            <Button
              className="agasnap--btn mt-5"
              type="primary"
              htmlType="submit"
            >
              Log In
            </Button>
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default Login;
