import React from "react";
import { Form, Input, Button } from "antd";

const ResetPasswordForm = ({ loading, onFinish, onFinishFailed }) => {
  return (
    <Form name="basic" onFinish={onFinish} onFinishFailed={onFinishFailed}>
      <div className="row">
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: "Email can not be empty",
              },
            ]}
          >
            <Input
              className="agasnap--input"
              size="large"
              placeholder="Email"
            />
          </Form.Item>
        </div>

        <div className="col-lg-4 col-md-4 col-12 offset-md-5">
          <Form.Item>
            {loading ? (
              <Button
                disabled
                className="agasnap--btn mt-5"
                type="primary"
                htmlType="submit"
              >
                Wait...
              </Button>
            ) : (
              <Button
                className="agasnap--btn mt-5"
                type="primary"
                htmlType="submit"
              >
                Submit
              </Button>
            )}
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default ResetPasswordForm;
