import React, { useState } from "react";
import { Form, Input, Button, Avatar } from "antd";

const UserList = ["Avatar 1", "Avatar 2", "Avatar 3", "Avatar 4"];
const ColorList = ["#f56a00", "#7265e6", "#ffbf00", "#00a2ae"];

const EditProfileForm = ({
  form,
  initialValues,
  onFinish,
  onFinishFailed,
  handleUpdate,
}) => {
  const [user, setUser] = useState(UserList[0]);
  const [color, setColor] = useState(ColorList[0]);
  const changeAvatar = () => {
    const index = UserList.indexOf(user);
    setUser(index < UserList.length - 1 ? UserList[index + 1] : UserList[0]);
    setColor(
      index < ColorList.length - 1 ? ColorList[index + 1] : ColorList[0]
    );
  };

  return (
    <Form
      form={form}
      name="control-hooks"
      initialValues={initialValues}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <div className="row">
        <div className="col-lg-12 col-md-12 col-12 mb-5">
          <Avatar
            style={{
              backgroundColor: color,
              verticalAlign: "middle",
              width: "100px",
              height: "100px",
            }}
            size="large"
          >
            {user}
          </Avatar>
          <Button
            size="small"
            style={{ margin: "0 16px", verticalAlign: "middle" }}
            onClick={changeAvatar}
          >
            Change avatar
          </Button>
        </div>
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="firstName"
            rules={[
              {
                required: true,
                message: "Please input your name",
              },
            ]}
          >
            <Input className="agasnap--input" placeholder="First name" />
          </Form.Item>
        </div>
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="lastName"
            rules={[
              {
                required: true,
                message: "Please input the last name",
              },
            ]}
          >
            <Input className="agasnap--input" placeholder="Last name" />
          </Form.Item>
        </div>
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input password",
              },
            ]}
          >
            <Input className="agasnap--input" placeholder="Password" />
          </Form.Item>
        </div>

        <div className="col-lg-6 col-md-6 col-12">
          <Form.Item>
            <Button
              className="agasnap--btn mt-5"
              type="primary"
              htmlType="submit"
              onClick={handleUpdate}
            >
              Save changes
            </Button>
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default EditProfileForm;
