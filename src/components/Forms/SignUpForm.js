import React from "react";
import { Form, Input, Button, message } from "antd";
import { useHistory } from "react-router-dom";
import { routes } from "../../config/routes";

const SignUp = ({
  loading,
  errorMessage,
  successMessage,
  onFinish,
  onFinishFailed,
}) => {
  const history = useHistory();
  if (successMessage) {
    message.success("Account was successfully created");
    history.push(routes.Login.url);
  }
  if (errorMessage) {
    message.error(errorMessage);
  }

  return (
    <Form
      name="basic"
      layout="vertical"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <div className="row">
        <div className="col-lg-6 col-md-6 col-12">
          <Form.Item
            name="firstName"
            rules={[
              {
                required: true,
                message: "Please input your First Name",
              },
            ]}
          >
            <Input className="agasnap--input" placeholder="First Name" />
          </Form.Item>
        </div>
        <div className="col-lg-6 col-md-6 col-12">
          <Form.Item
            name="lastName"
            rules={[
              {
                required: true,
                message: "Please input your Last name",
              },
            ]}
          >
            <Input className="agasnap--input" placeholder="Last name" />
          </Form.Item>
        </div>
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Please input your username",
              },
            ]}
          >
            <Input className="agasnap--input" placeholder="Username" />
          </Form.Item>
        </div>
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                type: "email",
              },
            ]}
          >
            <Input className="agasnap--input" placeholder="Email" />
          </Form.Item>
        </div>
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                min: 6,
                pattern: new RegExp(/(?=.*[A-Z])[a-zA-Z0-9!?]{6,}/),
                message: "At least 6 characters with one upper case letter",
              },
            ]}
          >
            <Input.Password
              className="agasnap--input"
              placeholder="Password"
              iconRender={(visible) => (visible ? "Hide" : "Show")}
            />
          </Form.Item>
        </div>

        <div className="col-lg-4 col-md-4 col-12 offset-md-5">
          {loading ? (
            <Form.Item>
              <Button
                disabled
                className="agasnap--btn mt-5"
                type="primary"
                htmlType="submit"
              >
                Wait...
              </Button>
            </Form.Item>
          ) : (
            <Form.Item>
              <Button
                className="agasnap--btn mt-5"
                type="primary"
                htmlType="submit"
              >
                Sign Up
              </Button>
            </Form.Item>
          )}
        </div>
      </div>
    </Form>
  );
};

export default SignUp;
