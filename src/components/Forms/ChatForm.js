import React from "react";
import { Form, Input } from "antd";

const ChatForm = ({ onFinish, onFinishFailed }) => {
  return (
    <Form name="basic" onFinish={onFinish} onFinishFailed={onFinishFailed}>
      <div className="row">
        <div className="col-lg-12 col-md-12 col-12">
          <Form.Item
            name="message"
            rules={[
              {
                required: true,
                message: "Type something",
              },
            ]}
          >
            <Input placeholder="Send a chat" />
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default ChatForm;
