import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import { message } from "antd";
import SignUpForm from "../../components/Forms/SignUpForm";
import { getUsers, postUser } from "../../redux/actions/user.actions";
import { routes } from "../../config/routes";
import logo from "../../assets/images/snapchat-icon.svg";
import "./CreateAccount.scss";

const CreateAccount = () => {
  const dispatch = useDispatch();
  const { successMessage, loading, errorMessage } = useSelector(
    (state) => state.PostUser
  );
  const { /*loading: getUsersloading,*/ data } = useSelector(
    (state) => state.Users
  );

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const onFinish = (values) => {
    const userInfo = data?.filter(
      (user) => user?.username === values?.username
    );
    const username = userInfo?.map((user) => user?.username);

    if (username.length === 0) {
      dispatch(postUser(values, username));
    } else {
      message.error("That username is in use, try another one");
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  let onFailSoHard = function (e) {
    console.log(e);
  };

  navigator.getUserMedia(
    { video: true, audio: true },
    function (localMediaStream) {
      var video = document.querySelector("video");
      video.src = window.URL.createObjectURL(localMediaStream);
      video.onloadedmetadata = function (e) {
        console.log(e);
      };
    },
    onFailSoHard
  );

  let user = JSON.parse(localStorage.getItem("user"));

  return (
    <>
      {user ? (
        <Redirect to={routes.Home.url} />
      ) : (
        <div className="agasnap--container--fluid">
          <div className="agasnap--container">
            <div className="agasnap--form--container">
              <div className="agasnap--signup">
                <div className="form-header">
                  <img src={logo} alt="" />
                  <h6 className="title">Sign Up for Aga-Snap</h6>
                </div>
                <SignUpForm
                  loading={loading}
                  errorMessage={errorMessage}
                  successMessage={successMessage}
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                />
              </div>
              <div className="switch--forms">
                <span>Already have Aga-Snap?</span>
                <Link className="link" to={routes.Login.url}>
                  Login In
                </Link>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default CreateAccount;
