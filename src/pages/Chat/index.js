import React from "react";
import { Button } from "antd";
import { useHistory } from "react-router-dom";
import ChatMessage from "../../components/ChatMessage";
import ChatForm from "../../components/Forms/ChatForm";
import { routes } from "../../config/routes";
import "./Chat.scss";

const Chat = () => {
  const history = useHistory();
  const handleBackClick = () => {
    history.push(routes.Home.url);
  };

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="agasnap--container--fluid">
      <div className="agasnap--container">
        <div className="agasnap--chat--container">
          <div className="agasnap--content">
            <div className="content--header">
              <Button onClick={handleBackClick}>Back</Button>
            </div>
            {[...Array(10)].map((_, index) => (
              <ChatMessage key={index} />
            ))}
            <ChatForm onFinish={onFinish} onFinishFailed={onFinishFailed} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Chat;
