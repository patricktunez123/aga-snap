import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import ResetPasswordForm from "../../components/Forms/ResetPasswordForm";
import { getUsers, resetRequest } from "../../redux/actions/user.actions";
import { routes } from "../../config/routes";
import logo from "../../assets/images/snapchat-icon.svg";
import "./PasswordResetRequest.scss";

const PasswordResetRequest = () => {
  const dispatch = useDispatch();
  const { successMessage, loading, errorMessage } = useSelector(
    (state) => state.PasswordReset
  );
  const { /*loading: getUsersloading,*/ data } = useSelector(
    (state) => state.Users
  );

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const onFinish = (values) => {
    const userInfo = data?.filter((user) => user?.email === values?.email);
    const email = userInfo?.map((user) => user?.email);
    if (email.length === 0) {
      message.error("That email is not found");
    } else {
      dispatch(resetRequest(values));
    }
  };

  if (successMessage) {
    message.success(successMessage);
  } else if (errorMessage) {
    message.error(errorMessage);
  }
  return (
    <div className="agasnap--container--fluid">
      <div className="agasnap--container">
        <div className="agasnap--form--container">
          <div className="agasnap--login">
            <div className="form-header">
              <img src={logo} alt="" />
              <h6 className="title">Reset Password</h6>
              <p className="text-center">
                If you do not know your current password, you may change it.
              </p>
            </div>
            <ResetPasswordForm
              loading={loading}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
            />
          </div>
          <div className="switch--forms">
            <span>Remember password?</span>
            <Link className="link" to={routes.Login.url}>
              Login
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PasswordResetRequest;
