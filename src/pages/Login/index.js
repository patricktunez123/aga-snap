import React from "react";
import { useHistory, Link, Redirect } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { message } from "antd";
import LoginForm from "../../components/Forms/LoginForm";
import { routes } from "../../config/routes";
import { Auth } from "../../redux/actions/auth.actions";
import logo from "../../assets/images/snapchat-icon.svg";
import "./Login.scss";

const Login = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { loading, errorMessage } = useSelector((state) => state.Auth);
  let user = JSON.parse(localStorage.getItem("user"));

  const onFinish = (values) => {
    if (
      values?.username === user?.email &&
      values?.password === user?.password
    ) {
      history.push(routes.Home.url);
    } else {
      message.error("Invalid email or password");
    }
    dispatch(Auth(values?.email, values?.password));
  };

  return (
    <>
      {user ? (
        <Redirect to={routes.Home.url} />
      ) : (
        <div className="agasnap--container--fluid">
          <div className="agasnap--container">
            <div className="agasnap--form--container">
              <div className="agasnap--login">
                <div className="form-header">
                  <img src={logo} alt="" />
                  <h6 className="title">Log in to Aga-Snap</h6>
                </div>
                <LoginForm
                  loading={loading}
                  errorMessage={errorMessage}
                  onFinish={onFinish}
                />
              </div>
              <div className="switch--forms">
                <span>New to Aga-Snap?</span>
                <Link className="link" to={routes.createAccount.url}>
                  Sign Up
                </Link>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Login;
