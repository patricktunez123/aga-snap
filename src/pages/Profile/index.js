import React, { useState } from "react";
import { Button } from "antd";
import { useHistory } from "react-router-dom";
import EditProfile from "../../components/Profile/EditProfile";
import { routes } from "../../config/routes";
import "./Profile.scss";

const Profile = () => {
  const history = useHistory();
  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  const handleBackClick = () => {
    history.push(routes.Home.url);
  };
  let user = JSON.parse(localStorage.getItem("user"));
  return (
    <div className="agasnap--container--fluid">
      <div className="agasnap--container">
        <div className="agasnap--profile--container">
          <div className="agasnap--profile">
            <div className="profile--header">
              <Button onClick={handleBackClick}>Back</Button>
              <Button onClick={showDrawer}>Edit profile</Button>
            </div>
            <EditProfile onClose={onClose} visible={visible} />
            <div>
              <span>Names: </span>
              <span>
                {user?.firstName} {user?.lastName}
              </span>
            </div>
            <div>
              <span>Username: </span>
              <span>{user?.username}</span>
            </div>

            <div>
              <span>Email: </span>
              <span>{user?.email}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profile;
