import React from "react";
import { Button } from "antd";
import { useHistory } from "react-router-dom";
import FriendsToAdd from "../../components/Lists/FriendsToAdd";
import { routes } from "../../config/routes";
import "./Friends.scss";

const Friends = () => {
  const history = useHistory();
  const handleBackClick = () => {
    history.push(routes.Home.url);
  };
  return (
    <div className="agasnap--container--fluid">
      <div className="agasnap--container">
        <div className="agasnap--content">
          <div className="content--header">
            <Button onClick={handleBackClick}>Back</Button>
          </div>
          <div className="agasnap-list">
            {[...Array(10)].map((_, index) => (
              <FriendsToAdd key={index} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Friends;
