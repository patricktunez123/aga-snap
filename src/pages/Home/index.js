import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "antd";
import { useHistory } from "react-router-dom";
import Contact from "../../components/Lists/Contact";
import { getFriends } from "../../redux/actions/friend.actions";
import { routes } from "../../config/routes";
import "./Home.scss";

const Home = () => {
  const dispatch = useDispatch();

  const { loading, data } = useSelector((state) => state.Friends);

  useEffect(() => {
    dispatch(getFriends());
  }, [dispatch]);

  const history = useHistory();

  const handleProfileClick = () => {
    history.push(routes.Profile.url);
  };
  const handleLogout = () => {
    localStorage.removeItem("user");
    history.push(routes.Login.url);
  };

  return (
    <div className="agasnap--container--fluid">
      <div className="agasnap--container">
        <div className="agasnap--content">
          <div className="content--header">
            <Button onClick={handleProfileClick}>Profile</Button>
            <div className="btn-group">
              <Button onClick={handleLogout}>Logout</Button>
            </div>
          </div>
          <div className="agasnap-list">
            {loading
              ? "loading"
              : data &&
                data?.map((friend) => <Contact key={friend?.id} {...friend} />)}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
