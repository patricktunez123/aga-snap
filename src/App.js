import React, { lazy, Fragment, Suspense } from "react";
import { Switch, Route } from "react-router-dom";
import { Spin } from "antd";
import Layout from "./components/Layout";
import { routes } from "./config/routes";

const CreateAccount = lazy(() => import("./pages/CreateAccount"));
const Login = lazy(() => import("./pages/Login"));
const Home = lazy(() => import("./pages/Home"));
const Friends = lazy(() => import("./pages/Friends"));
const Chat = lazy(() => import("./pages/Chat"));
const Profile = lazy(() => import("./pages/Profile"));
const PasswordResetRequest = lazy(() => import("./pages/PasswordResetRequest"));
const ResetPassword = lazy(() => import("./pages/ResetPassword"));
const NotFound = lazy(() => import("./pages/NotFound"));

function App() {
  return (
    <Fragment>
      <Suspense
        fallback={
          <Layout>
            <div
              style={{
                height: "100vh",
                width: "100%",
                textAlign: "center",
                paddingTop: "calc(50vh - 7px)",
              }}
            >
              <Spin />
            </div>
          </Layout>
        }
      >
        <Switch>
          <Route path={routes.createAccount.url} exact>
            <Layout>
              <CreateAccount />
            </Layout>
          </Route>
          <Route path={routes.Login.url} exact>
            <Layout>
              <Login />
            </Layout>
          </Route>
          <Route path={routes.Home.url} exact>
            <Layout>
              <Home />
            </Layout>
          </Route>
          <Route path={routes.Friends.url} exact>
            <Layout>
              <Friends />
            </Layout>
          </Route>
          <Route path={routes.Chat.url} exact>
            <Layout>
              <Chat />
            </Layout>
          </Route>
          <Route path={routes.Profile.url} exact>
            <Layout>
              <Profile />
            </Layout>
          </Route>
          <Route path={routes.PasswordResetRequest.url} exact>
            <Layout>
              <PasswordResetRequest />
            </Layout>
          </Route>
          <Route path={routes.ResetPassword.url} exact>
            <Layout>
              <ResetPassword />
            </Layout>
          </Route>
          <Route>
            <Layout>
              <NotFound />
            </Layout>
          </Route>
        </Switch>
      </Suspense>
    </Fragment>
  );
}

export default App;
